﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            Console.WriteLine("Введите размер массива");
            n = int.Parse(Console.ReadLine());

            double[] mas = new double[n];
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                mas[i] = rnd.Next(1, 50);
            }

            for (int i = 0; i < n; i++)
            {
                if (mas[i] > 10)
                {

                    mas[i] = Math.Sqrt(mas[i]);
                }
            }

            for (int i = 0; i < n; i++)
            {

                Console.WriteLine($"{i + 1} = {mas[i]}");
            }

            Console.ReadKey();

        }
    }
}

