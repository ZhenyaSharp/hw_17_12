﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int stud = 22, r, num = 0;

            Console.WriteLine("Введите значение r: ");
            r = int.Parse(Console.ReadLine());

            int[] mas = new int[stud];

            for (int i = 0; i < stud; i++)
            {
                mas[i] = rnd.Next(160, 190);
            }

            for (int i = 0; i < stud; i++)
            {
                if (mas[i] < r)
                {
                    num++;
                }
                Console.WriteLine($"{i + 1} = {mas[i]}");
            }

            Console.WriteLine($"{num} человека в классе ниже {r} см");
            Console.ReadKey();
        }
    }
}
