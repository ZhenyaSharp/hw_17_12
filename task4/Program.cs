﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            int n;
            Console.WriteLine("Введите размер массива");
            n = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int[] mas = new int[n];
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                mas[i] = rnd.Next(1, 50);
            }

            for (int i = 0; i < n; i++)
            {
                if (mas[i] <= 20)
                {
                    sum += mas[i];
                }
                Console.WriteLine($"{i + 1} = {mas[i]}, ");
            }
            Console.WriteLine();
            Console.WriteLine($"сумма всех элементов не больше 20 = {sum}");
            Console.ReadKey();
        }
    }
}
