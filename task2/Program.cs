﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            Console.WriteLine("Введите размер массива");
            n = int.Parse(Console.ReadLine());

            int[] mas = new int[n];
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                mas[i] = rnd.Next(1, 100);
            }

            for (int i = 0; i < n; i++)
            {
                if (i % 10 == 0)
                {
                    Console.WriteLine($"{i} = {mas[i]}");
                }
            }
            Console.ReadKey();
        }
    }
}
